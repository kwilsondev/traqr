﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using TraQR.Mobile;
namespace TraQR.Mobile.Tests
{
    [TestFixture()]
    public class Test
    {
        [Test()]
        public void TestAppNotNull()
        {
            var app = new App();
            Assert.NotNull(app);
        }

        [Test()]
        public void TestAPICategoryType()
        {
            ApiAccess api = new ApiAccess();
            Assert.IsInstanceOf<List<Models.traqr_category>>(api.GetCategories());
        }

        [Test()]
        public void TestAPICategoryGet()
        {
            ApiAccess api = new ApiAccess();
            Assert.IsNotNull(api.GetCategories());
        }

        [Test()]
        public void TestAPIItemType()
        {
            ApiAccess api = new ApiAccess();
            Assert.IsInstanceOf<List<Models.traqr_item>>(api.GetItems());
        }

        [Test()]
        public void TestAPIItemGet()
        {
            ApiAccess api = new ApiAccess();
            Assert.IsNotNull(api.GetItems());
        }

        [Test()]
        public void TestAPIInvType()
        {
            ApiAccess api = new ApiAccess();
            Assert.IsInstanceOf<List<Models.traqr_inventory>>(api.GetInventory());
        }

        [Test()]
        public void TestAPIInvGet()
        {
            ApiAccess api = new ApiAccess();
            Assert.IsNotNull(api.GetInventory());
        }

        [Test()]
        public void TestAPIStatusType()
        {
            ApiAccess api = new ApiAccess();
            Assert.IsInstanceOf<List<Models.traqr_status>>(api.GetStatuses());
        }

        [Test()]
        public void TestAPIStatusGet()
        {
            ApiAccess api = new ApiAccess();
            Assert.IsNotNull(api.GetStatuses());
        }

        [Test()]
        public void TestAPIAuthenticateEmptyUserPass()
        {
            ApiAccess api = new ApiAccess();
            Assert.IsNull(api.Authenticate("",""));
        }

        [Test()]
        public void TestAPIAuthenticateNullUserPass()
        {
            ApiAccess api = new ApiAccess();
            Assert.IsNull(api.Authenticate(null, null));
        }

        [Test()]
        public void TestAPIPasswordHash()
        {
            ApiAccess api = new ApiAccess();
            string password = "fortnite1", username = "J.Bilitski";
            Models.traqr_user user = api.Authenticate(username, password);
            Assert.AreNotEqual(password, user.Password);
        }
    }
}
