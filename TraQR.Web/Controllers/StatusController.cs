﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TraQR.Web.Models;

namespace TraQR.Web.Controllers
{
    public class StatusController : Controller
    {
        private TraQREntities db = new TraQREntities();

        // GET: Status
        public ActionResult Index()
        {
            return View(db.traqr_status.ToList());
        }

        

        // GET: Status/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            traqr_status traqr_status = db.traqr_status.Find(id);
            if (traqr_status == null)
            {
                return HttpNotFound();
            }
            return View(traqr_status);
        }

        // GET: Status/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Status/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StatusID,Status")] traqr_status traqr_status)
        {
            if (ModelState.IsValid)
            {
                db.traqr_status.Add(traqr_status);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(traqr_status);
        }

        // GET: Status/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            traqr_status traqr_status = db.traqr_status.Find(id);
            if (traqr_status == null)
            {
                return HttpNotFound();
            }
            return View(traqr_status);
        }

        // POST: Status/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "StatusID,Status")] traqr_status traqr_status)
        {
            if (ModelState.IsValid)
            {
                db.Entry(traqr_status).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(traqr_status);
        }

        // GET: Status/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            traqr_status traqr_status = db.traqr_status.Find(id);
            if (traqr_status == null)
            {
                return HttpNotFound();
            }
            return View(traqr_status);
        }

        // POST: Status/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            traqr_status traqr_status = db.traqr_status.Find(id);
            db.traqr_status.Remove(traqr_status);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
