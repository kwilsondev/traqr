﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using TraQR.Web.Models;


namespace TraQR.Web.Controllers
{
    public class UserController : ApiController
    {
        [System.Web.Http.HttpPost]
        public List<traqr_user> GetAll()
        {
            using (var db = new TraQREntities())
            {
                var allItems = db.traqr_user;
                return allItems.ToList();
            }
        }

        [System.Web.Http.HttpPost]
        public traqr_user GetByUserID()
        {
            int theUserID = Convert.ToInt32(Request.Headers.GetValues("UserID").FirstOrDefault());

            using (var db = new TraQREntities())
            {
                var UserIDs = db.traqr_user.Where(u => u.UserID == theUserID);
                return UserIDs.FirstOrDefault();
            }
        }

        [System.Web.Http.HttpPost]
        public traqr_user GetByComanyID()
        {
            int theCompanyID = Convert.ToInt32(Request.Headers.GetValues("CompanyID").FirstOrDefault());

            using (var db = new TraQREntities())
            {
                var CompanyIDs = db.traqr_user.Where(u => u.UserID == theCompanyID);
                return CompanyIDs.FirstOrDefault();
            }
        }

        [System.Web.Http.HttpPost]
        public traqr_user GetByUsername()
        {
            string theUsername = Request.Headers.GetValues("Username").FirstOrDefault();

            using (var db = new TraQREntities())
            {
                var Usernames = db.traqr_user.Where(u => u.Username == theUsername);
                return Usernames.FirstOrDefault();
            }
        }
    }
}
