﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using TraQR.Web.Models;

namespace TraQR.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var mvcName = typeof(Controller).Assembly.GetName();
            var isMono = Type.GetType("Mono.Runtime") != null;

            ViewData["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
            ViewData["Runtime"] = isMono ? "Mono" : ".NET";

                return View();
        }

        [System.Web.Http.AcceptVerbs("Post")]
        public ActionResult SetLogin()
        {
            //get username and from the request
            var user = Request.Headers.GetValues("User").FirstOrDefault();

            Session["CurrentUser"] = Request.Headers.GetValues("User").FirstOrDefault();

            return new JsonResult{ Data = "Finish"};
        }
    }
}
