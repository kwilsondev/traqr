﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Services;
using TraQR.Web.Models;

namespace TraQR.Web.Controllers
{
    public class ItemController : ApiController
    {
        [System.Web.Http.AcceptVerbs("Post")]
        public string update()
        {
            //get data from the request
            int itemID = int.Parse(Request.Headers.GetValues("ItemID").FirstOrDefault());
            string itemName = Request.Headers.GetValues("ItemName").FirstOrDefault();
            string itemDescription = Request.Headers.GetValues("ItemDescription").FirstOrDefault();
            int categoryID = int.Parse(Request.Headers.GetValues("CategoryID").FirstOrDefault());
            decimal latitude  = decimal.Parse(Request.Headers.GetValues("Latitude").FirstOrDefault());
            decimal longitude = decimal.Parse(Request.Headers.GetValues("Longitude").FirstOrDefault());
            string locationName = Request.Headers.GetValues("LocationDescription").FirstOrDefault();
            int statusID = int.Parse(Request.Headers.GetValues("StatusID").FirstOrDefault());
            string statusDescription = Request.Headers.GetValues("StatusDescription").FirstOrDefault();
            int companyID = int.Parse(Request.Headers.GetValues("CompanyID").FirstOrDefault());

            using (var db = new TraQREntities())
            {
                var item = db.traqr_items.Where(i => i.ItemID == itemID).FirstOrDefault();

                //update the data from the header values
                item.ItemName = itemName;
                item.ItemDescription = itemDescription;
                item.CategoryID = categoryID;
                item.Latitude = latitude;
                item.Longitude = longitude;
                item.LocationName = locationName;
                item.StatusID = statusID;
                item.StatusDescription = statusDescription;
                item.CompanyID = companyID;

                db.SaveChanges();
            }

            return "";
        }
       
        [System.Web.Http.HttpPost]
        public List<traqr_items> GetAll()
        {
            using (var db = new TraQREntities())
            {
                var allItems = db.traqr_items.Select(I => I);
                return allItems.ToList();
            }

        }
        
        [System.Web.Http.HttpPost]
        public List<traqr_items> GetInventory()
        {
            var head = Request.Headers.GetValues("CompanyID").FirstOrDefault();
            int theCompanyID = Convert.ToInt32(JsonConvert.DeserializeObject<traqr_user>(head.ToString()).CompanyID);
            using (var db = new TraQREntities())
            {
                //var query = from i in db.traqr_items
                //where i.CompanyID == theCompanyID
                //select i;
                
                var inventory = db.traqr_items
                       .Where(s => s.CompanyID == theCompanyID).ToList();

                return inventory;
            }

        }

        [System.Web.Http.HttpPost]
        public traqr_items GetByCompanyID()
        {

            int theCompanyID = Convert.ToInt32(Request.Headers.GetValues("CompanyID").FirstOrDefault());

            using (var db = new TraQREntities())
            {
                var companyItems = db.traqr_items.Where(u => u.CompanyID == theCompanyID);
                return companyItems.FirstOrDefault();
            }
        }

        [System.Web.Http.HttpPost]
        public traqr_items GetByItemID()
        {
            int theID = Convert.ToInt32(Request.Headers.GetValues("ItemID").FirstOrDefault());

            using (var db = new TraQREntities())
            {
                var IdItems = db.traqr_items.Where(u => u.ItemID == theID);
                return IdItems.FirstOrDefault();
            }
        }

        [System.Web.Http.HttpPost]
        public traqr_items GetByLocationID()
        {

            string theID = Request.Headers.GetValues("LocationID").FirstOrDefault();

            using (var db = new TraQREntities())
            {
                var LocationItems = db.traqr_items.Where(u => u.LocationName == theID);
                return LocationItems.FirstOrDefault();
            }
        }

        [System.Web.Http.HttpPost]
        public traqr_items GetByStatus()
        {
            int theStatus = Convert.ToInt32(Request.Headers.GetValues("StatusID").FirstOrDefault());

            using (var db = new TraQREntities())
            {
                var StatusItems = db.traqr_items.Where(u => u.StatusID == theStatus);
                return StatusItems.FirstOrDefault();
            }
        }

        [System.Web.Http.HttpPost]
        public void AddItem()
        {
            using (var db = new TraQREntities())
            {
                traqr_items newItem = new traqr_items();
                newItem.ItemID = Convert.ToInt32(Request.Headers.GetValues("ItemID").FirstOrDefault());
                newItem.ItemName = Request.Headers.GetValues("ItemName").FirstOrDefault();
                newItem.ItemDescription = Request.Headers.GetValues("ItemDescription").FirstOrDefault();
                newItem.CategoryID = Convert.ToInt32(Request.Headers.GetValues("CategoryID").FirstOrDefault());
                newItem.Latitude = Convert.ToDecimal(Request.Headers.GetValues("Latitude").FirstOrDefault());
                newItem.Longitude = Convert.ToDecimal(Request.Headers.GetValues("Longitude").FirstOrDefault());
                newItem.UserID = Convert.ToInt32(Request.Headers.GetValues("UserID").FirstOrDefault());
                newItem.OrderNumber = Convert.ToInt32(Request.Headers.GetValues("OrderNumber").FirstOrDefault());
                newItem.LocationName = Request.Headers.GetValues("LocationName").FirstOrDefault();
                newItem.LocationDescription = Request.Headers.GetValues("LocationDescription").FirstOrDefault();
                newItem.StatusID = Convert.ToInt32(Request.Headers.GetValues("StatusID").FirstOrDefault());
                newItem.StatusDescription = Request.Headers.GetValues("StatusDescription").FirstOrDefault();
                newItem.CompanyID = Convert.ToInt32(Request.Headers.GetValues("CompanyID").FirstOrDefault());

                var matchingitemname = db.traqr_items.Where(u => u.ItemName == Request.Headers.GetValues("ItemName").FirstOrDefault());
                var matchingcompany = db.traqr_items.Where(u => u.CompanyID == Convert.ToInt32(Request.Headers.GetValues("CompanyID").FirstOrDefault()));

                if (matchingitemname.Count() >= 1 && matchingcompany.Count() >= 1)
                {
                    Console.WriteLine("Item already exists.");
                    return;
                }
                else
                {
                    db.traqr_items.Add(newItem);
                    return;
                }

            }
        }

        [System.Web.Http.HttpPost]
        public ActionResult EditItem()
        {
            return new JsonResult { Data = "{ Called Function: 'EditItem' }", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        [System.Web.Http.HttpPost]
        public void DeleteItem(traqr_items itembeingchecked)
        {
            using (var db = new TraQREntities())
            {
                string matchingitemname = Request.Headers.GetValues("LocationID").FirstOrDefault();
                int matchingcompany = Convert.ToInt32(Request.Headers.GetValues("CompanyID").FirstOrDefault());

                if (matchingitemname == itembeingchecked.ItemName && matchingcompany == itembeingchecked.CompanyID)
                {
                    db.traqr_items.Remove(itembeingchecked);
                    return;
                }
            }
        }
    }
}
