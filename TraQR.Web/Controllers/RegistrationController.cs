﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TraQR.Web.Models;

namespace TraQR.Web.Controllers
{
    public class RegistrationController : ApiController
    {

        [System.Web.Http.AcceptVerbs("Post")]
        public traqr_user Authenticate()
        {
            //get username and password from the request
            string username = Request.Headers.GetValues("UserName").FirstOrDefault();
            string password = Request.Headers.GetValues("Password").FirstOrDefault();

            using (var db = new TraQREntities())
            {
                //get a list of users with matching credentials
                var matchingUsers = db.traqr_user.Where(u => u.Username == username && u.Password == password);

                //if a match is found, return the user info for the sign in
                if (matchingUsers.Count() == 1)
                {
                    return matchingUsers.First();
                }

                //no match found, return an empty user with a userID of -1
                return new traqr_user { UserID = -1};

            }

        }

        [System.Web.Http.AcceptVerbs("Post")]
        public string RegisterUser()
        {
            //Get the potential new user data from the POST request header data
            traqr_user newUser = new traqr_user();
            newUser.FirstName = Request.Headers.GetValues("FirstName").FirstOrDefault();
            newUser.LastName = Request.Headers.GetValues("LastName").FirstOrDefault();
            newUser.Username = Request.Headers.GetValues("UserName").FirstOrDefault();

            string firstPassword = Request.Headers.GetValues("FirstPassword").FirstOrDefault();
            string secondPassword = Request.Headers.GetValues("SecondPassword").FirstOrDefault();
            string registrationCode = Request.Headers.GetValues("RegistrationCode").FirstOrDefault();

            //check for bad password inputs
            if (firstPassword != secondPassword)
            {
                return "Error: Passwords did not match.";
            }
            else
            {
                newUser.Password = firstPassword;
            }

            int nameMatches = 0;

            using (var db = new TraQREntities())
            {
                //get the number of users in the db with this username
                nameMatches = db.traqr_user.Where(u => u.Username == newUser.Username).Count();

                //get the number of matching company codes from the db
                var companyMatches = db.traqr_companies.Where(c => c.RegistrationCode == registrationCode);

                //if no matching names exist and company code is found, add the user to the db
                if (nameMatches == 0 && companyMatches.Count() == 1)
                {
                    newUser.CompanyID = companyMatches.First().CompanyID;

                    db.traqr_user.Add(newUser);
                    db.SaveChanges();
                    return "New user " + newUser.Username + " created.";
                }

            }
            return "Failed to create new user " + newUser.Username;
        }

        [System.Web.Http.AcceptVerbs("Post")]
        public string RegisterCompany()
        {

            //Get the potential new company data from the POST request header data
            traqr_companies newCompany = new traqr_companies();
            newCompany.CompanyName = Request.Headers.GetValues("CompanyName").FirstOrDefault();
            newCompany.RegistrationCode = Request.Headers.GetValues("RegistrationCode").FirstOrDefault();
            newCompany.Street = Request.Headers.GetValues("Street").FirstOrDefault();
            newCompany.City = Request.Headers.GetValues("City").FirstOrDefault();
            newCompany.State = Request.Headers.GetValues("State").FirstOrDefault();
            newCompany.Zip = Request.Headers.GetValues("Zip").FirstOrDefault();
            newCompany.AptNumber = Request.Headers.GetValues("AptNumber").FirstOrDefault();

            int nameMatches = 0;

            using (var db = new TraQREntities())
            {
                //get the current company names from the db
                nameMatches = db.traqr_companies.Where(c => c.CompanyName == newCompany.CompanyName).Count();

                //if no matching names exist, add the company to the db
                if (nameMatches == 0)
                {
                    db.traqr_companies.Add(newCompany);
                    db.SaveChanges();
                    return "New company " + newCompany.CompanyName + " created.";
                }

            }

            return "Failed to create" + newCompany.CompanyName;
        }

    }
}