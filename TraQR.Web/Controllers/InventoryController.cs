﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TraQR.Web.Models;
using Newtonsoft.Json;

namespace TraQR.Web.Controllers
{
    public class InventoryController : Controller
    {
        private TraQREntities db = new TraQREntities();

        // GET: Inventory
        public ActionResult Index()
        {

            //get session data
            var CurrentUser = JsonConvert.DeserializeObject<traqr_user>(Session["CurrentUser"].ToString());

            //Gets companyID of logged in user
            var companyID = (from u in db.traqr_user
                             where u.Username == CurrentUser.Username
                             select u.CompanyID).FirstOrDefault();

            //creats new table from joins of Entity Framework model of SQL TraQR DB
            var newModel = (from i in db.traqr_items
                            join co in db.traqr_companies on i.CompanyID equals co.CompanyID
                            join u in db.traqr_user on i.UserID equals u.UserID
                            join s in db.traqr_status on i.StatusID equals s.StatusID
                            where i.CompanyID == companyID
                            select new
                            {
                                itemID = i.ItemID,
                                itemName = i.ItemName,
                                itemDescription = i.ItemDescription,
                                status = s.Status,
                                company = co.CompanyName,
                                username = u.Username
                            }
                         ).ToList();

            //Puts newModel table into List with DataType so View knows what type of model it's receiving
            List<InventoryViewModel> model = new List<InventoryViewModel>();
            foreach(var i in newModel)
            {
                InventoryViewModel item = new InventoryViewModel();
                item.Company = i.company;
                item.ItemDescription = i.itemDescription;
                item.ItemID = i.itemID;
                item.ItemName = i.itemName;
                item.Status = i.status;
                item.Username = i.username;

                model.Add(item);
            }
            
            return View(model);
        }
       

        // GET: Inventory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            traqr_items traqr_items = db.traqr_items.Find(id);
            if (traqr_items == null)
            {
                return HttpNotFound();
            }
            return View(traqr_items);
        }

        // GET: Inventory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inventory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ItemID,ItemName,ItemDescription,CategoryID,Latitude,Longitude,UserID,CompanyID,OrderNumber,LocationName,LocationDescription,StatusID,StatusDescription")] traqr_items traqr_items)
        {
            if (ModelState.IsValid)
            {
                APIController api = new APIController();
                db.traqr_items.Add(traqr_items);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(traqr_items);
        }

        // GET: Inventory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            traqr_items traqr_items = db.traqr_items.Find(id);
            if (traqr_items == null)
            {
                return HttpNotFound();
            }

            //var db = new InventoryModel();
            //var item = (from a in db.traqr_items
            //            where a.ItemID == id
            //            select a).FirstOrDefault();

            //if (item == null)
            //{
            //    return HttpNotFound();
            //}


            //var company = (from a in db.traqr_companies
            //               where a.CompanyID == item.CompanyID
            //               select a).FirstOrDefault();

            //var status = (from a in db.traqr_status
            //              select a).ToList();

            //var category = (from a in db.traqr_category
            //                select a).ToList();

            //var users = (from a in db.traqr_user
            //             where a.UserID == item.UserID
            //             select a).ToList();

            //var model = new InventoryViewModel { company_ = company, item_ = item, categories_ = category, status_ = status, users_ = users };


            return View(traqr_items);


        }

        // POST: Inventory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ItemID,ItemName,ItemDescription,CategoryID,CompanyID,Latitude,Longitude,UserID,OrderNumber,LocationName,LocationDescription,StatusID,StatusDescription")] traqr_items traqr_items)
        {
            if (ModelState.IsValid)
            {
                db.Entry(traqr_items).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(traqr_items);
        }

        // GET: Inventory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            traqr_items traqr_items = db.traqr_items.Find(id);
            if (traqr_items == null)
            {
                return HttpNotFound();
            }
            return View(traqr_items);
        }

        // POST: Inventory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            traqr_items traqr_items = db.traqr_items.Find(id);
            db.traqr_items.Remove(traqr_items);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

}
