﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using TraQR.Web.Models;

namespace TraQR.Web.Controllers
{
    public class APIController : ApiController
    {
        //-------------------------------------------------
        //| example method to return a simple JSON object |
        //|  when a call is made to localhost/api/print   |
        //-------------------------------------------------

        [System.Web.Http.HttpGet]    //exposes the method to GET requests
        [System.Web.Http.HttpPost]   //exposes the method to POST requests
        public ActionResult Print()
        {
            //create the JSON object to hold the results
            var json = new JsonResult();

            using (var db = new TraQREntities())
            {
                var data = db.traqr_items.Select(i => i).ToList();

                json.Data = data;
                json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            }

            return json; //return the JSON object to the client

        }

        [System.Web.Http.AcceptVerbs("Post")]
        public string RegisterUser()
        {
            //Get the potential new user data from the POST request header data
            traqr_user newUser = new traqr_user();
            newUser.FirstName = Request.Headers.GetValues("FirstName").FirstOrDefault();
            newUser.LastName = Request.Headers.GetValues("LastName").FirstOrDefault();
            newUser.Username = Request.Headers.GetValues("UserName").FirstOrDefault();

            string firstPassword = Request.Headers.GetValues("FirstPassword").FirstOrDefault();
            string secondPassword = Request.Headers.GetValues("SecondPassword").FirstOrDefault();
            string registrationCode = Request.Headers.GetValues("RegistrationCode").FirstOrDefault();

            //check for bad password inputs
            if (firstPassword != secondPassword)
            {
                return "Error: Passwords did not match.";
            }
            else
            {
                newUser.Password = firstPassword;
            }

            int nameMatches = 0;

            using (var db = new TraQREntities())
            {
                //get the number of users in the db with this username
                nameMatches = db.traqr_user.Where(u => u.Username == newUser.Username).Count();

                //get the number of matching company codes from the db
                var companyMatches = db.traqr_companies.Where(c => c.RegistrationCode == registrationCode);

                //if no matching names exist and company code is found, add the user to the db
                if (nameMatches == 0 && companyMatches.Count() == 1)
                {
                    newUser.CompanyID = companyMatches.First().CompanyID;

                    db.traqr_user.Add(newUser);
                    db.SaveChanges();
                    return "New user " + newUser.Username + " created.";
                }

            }
            return "Failed to create new user " + newUser.Username;
        }

        [System.Web.Http.AcceptVerbs("Post")]
        public string RegisterCompany()
        {

            //Get the potential new company data from the POST request header data
            traqr_companies newCompany = new traqr_companies();
            newCompany.CompanyName = Request.Headers.GetValues("CompanyName").FirstOrDefault();
            newCompany.RegistrationCode = Request.Headers.GetValues("RegistrationCode").FirstOrDefault();
            newCompany.Street = Request.Headers.GetValues("Street").FirstOrDefault();
            newCompany.City = Request.Headers.GetValues("City").FirstOrDefault();
            newCompany.State = Request.Headers.GetValues("State").FirstOrDefault();
            newCompany.Zip = Request.Headers.GetValues("Zip").FirstOrDefault();
            newCompany.AptNumber = Request.Headers.GetValues("AptNumber").FirstOrDefault();

            int nameMatches = 0;

            using (var db = new TraQREntities())
            {
                //get the current company names from the db
                nameMatches = db.traqr_companies.Where(c => c.CompanyName == newCompany.CompanyName).Count();

                //if no matching names exist, add the company to the db
                if (nameMatches == 0)
                {
                    db.traqr_companies.Add(newCompany);
                    db.SaveChanges();
                    return "New company " + newCompany.CompanyName + " created.";
                }

            }

            return "Failed to create" + newCompany.CompanyName;
        }

        public JsonResult traqr_items()
        {
            //create the JSON object to hold the results
            var json = new JsonResult();


            if (Request.Method.Equals(HttpMethod.Get))             //check if request is a GET request
            {

                json.Data = "{ result: 'Success using a GET request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here
                json.JsonRequestBehavior = JsonRequestBehavior.AllowGet; // needed in the json if accessed from a GET request only

            }
            else if (Request.Method.Equals(HttpMethod.Post))
            {    //check if request is a Post request

                json.Data = "{ result: 'Success using a POST request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }
            else
            {

                json.Data = "{ result: 'Bad request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }

            return json; //return the JSON object to the client

        }

        public JsonResult traqr_user()
        {
            //create the JSON object to hold the results
            var json = new JsonResult();


            if (Request.Method.Equals(HttpMethod.Get))             //check if request is a GET request
            {

                json.Data = "{ result: 'Success using a GET request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here
                json.JsonRequestBehavior = JsonRequestBehavior.AllowGet; // needed in the json if accessed from a GET request only

            }
            else if (Request.Method.Equals(HttpMethod.Post))
            {    //check if request is a Post request

                json.Data = "{ result: 'Success using a POST request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }
            else
            {

                json.Data = "{ result: 'Bad request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }

            return json; //return the JSON object to the client

        }

        public JsonResult traqr_category()
        {
            //create the JSON object to hold the results
            var json = new JsonResult();


            if (Request.Method.Equals(HttpMethod.Get))             //check if request is a GET request
            {

                json.Data = "{ result: 'Success using a GET request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here
                json.JsonRequestBehavior = JsonRequestBehavior.AllowGet; // needed in the json if accessed from a GET request only

            }
            else if (Request.Method.Equals(HttpMethod.Post))
            {    //check if request is a Post request

                json.Data = "{ result: 'Success using a POST request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }
            else
            {

                json.Data = "{ result: 'Bad request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }

            return json; //return the JSON object to the client

        }


        public JsonResult traqr_companies()
        {
            //create the JSON object to hold the results
            var json = new JsonResult();


            if (Request.Method.Equals(HttpMethod.Get))             //check if request is a GET request
            {

                json.Data = "{ result: 'Success using a GET request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here
                json.JsonRequestBehavior = JsonRequestBehavior.AllowGet; // needed in the json if accessed from a GET request only

            }
            else if (Request.Method.Equals(HttpMethod.Post))
            {    //check if request is a Post request

                json.Data = "{ result: 'Success using a POST request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }
            else
            {

                json.Data = "{ result: 'Bad request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }

            return json; //return the JSON object to the client

        }


        public JsonResult traqr_status()
        {
            //create the JSON object to hold the results
            var json = new JsonResult();


            if (Request.Method.Equals(HttpMethod.Get))             //check if request is a GET request
            {

                json.Data = "{ result: 'Success using a GET request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here
                json.JsonRequestBehavior = JsonRequestBehavior.AllowGet; // needed in the json if accessed from a GET request only

            }
            else if (Request.Method.Equals(HttpMethod.Post))
            {    //check if request is a Post request

                json.Data = "{ result: 'Success using a POST request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }
            else
            {

                json.Data = "{ result: 'Bad request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }

            return json; //return the JSON object to the client

        }
    }
}
