﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using TraQR.Web.Models;

namespace TraQR.Web.Controllers
{
    public class CompanyController : Controller
    {
        [System.Web.Http.HttpPost]
        public List<traqr_companies> GetAll()
        {
            using (var db = new TraQREntities())
            {
                var allItems = db.traqr_companies;
                return allItems.ToList();
            }
        }

        [System.Web.Http.HttpPost]
        public traqr_companies GetByCompanyID()
        {
            int theCompanyID = Convert.ToInt32(Request.Headers.GetValues("CompanyID").FirstOrDefault());

            using (var db = new TraQREntities())
            {
                var CompanyIDs = db.traqr_companies.Where(u => u.CompanyID == theCompanyID);
                return CompanyIDs.FirstOrDefault();
            }
        }

        [System.Web.Http.HttpPost]
        public traqr_companies GetByComanyName()
        {
            string theCompanyName = Request.Headers.GetValues("CompanyName").FirstOrDefault();

            using (var db = new TraQREntities())
            {
                var CompanyNames = db.traqr_companies.Where(u => u.CompanyName == theCompanyName);
                return CompanyNames.FirstOrDefault();
            }
        }

        [System.Web.Http.HttpPost]
        public traqr_companies GetByRegistrationCode()
        {
            string theRegistrationCode = Request.Headers.GetValues("RegistrationCode").FirstOrDefault();

            using (var db = new TraQREntities())
            {
                var RegistrationCodes = db.traqr_companies.Where(u => u.RegistrationCode == theRegistrationCode);
                return RegistrationCodes.FirstOrDefault();
            }
        }
    }
}