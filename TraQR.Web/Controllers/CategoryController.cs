﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TraQR.Web.Models;

namespace TraQR.Web.Controllers
{
    public class CategoryController : ApiController
    {
        private TraQREntities db = new TraQREntities();

        // GET: api/Category
        public IQueryable<traqr_category> GetAll()
        {
            return db.traqr_category;
        }

        // GET: api/Category/5
        [ResponseType(typeof(traqr_category))]
        public IHttpActionResult GetCategory(int id)
        {
            traqr_category traqr_category = db.traqr_category.Find(id);
            if (traqr_category == null)
            {
                return NotFound();
            }

            return Ok(traqr_category);
        }

        // PUT: api/Category/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttraqr_category(int id, traqr_category traqr_category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != traqr_category.CategoryID)
            {
                return BadRequest();
            }

            db.Entry(traqr_category).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!traqr_categoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Category
        [ResponseType(typeof(traqr_category))]
        public IHttpActionResult Posttraqr_category(traqr_category traqr_category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.traqr_category.Add(traqr_category);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = traqr_category.CategoryID }, traqr_category);
        }

        // DELETE: api/Category/5
        [ResponseType(typeof(traqr_category))]
        public IHttpActionResult Deletetraqr_category(int id)
        {
            traqr_category traqr_category = db.traqr_category.Find(id);
            if (traqr_category == null)
            {
                return NotFound();
            }

            db.traqr_category.Remove(traqr_category);
            db.SaveChanges();

            return Ok(traqr_category);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool traqr_categoryExists(int id)
        {
            return db.traqr_category.Count(e => e.CategoryID == id) > 0;
        }
    }
}