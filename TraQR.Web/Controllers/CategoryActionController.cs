﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TraQR.Web.Models;

namespace TraQR.Web.Controllers
{
    public class CategoryActionController : Controller
    {
        private TraQREntities db = new TraQREntities();

        // GET: CategoryAction
        public ActionResult Index()
        {
            return View(db.traqr_category.ToList());
        }

        // GET: CategoryAction/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            traqr_category traqr_category = db.traqr_category.Find(id);
            if (traqr_category == null)
            {
                return HttpNotFound();
            }
            return View(traqr_category);
        }

        // GET: CategoryAction/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CategoryAction/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoryID,CategoryName")] traqr_category traqr_category)
        {
            if (ModelState.IsValid)
            {
                db.traqr_category.Add(traqr_category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(traqr_category);
        }

        // GET: CategoryAction/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            traqr_category traqr_category = db.traqr_category.Find(id);
            if (traqr_category == null)
            {
                return HttpNotFound();
            }
            return View(traqr_category);
        }

        // POST: CategoryAction/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoryID,CategoryName")] traqr_category traqr_category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(traqr_category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(traqr_category);
        }

        // GET: CategoryAction/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            traqr_category traqr_category = db.traqr_category.Find(id);
            if (traqr_category == null)
            {
                return HttpNotFound();
            }
            return View(traqr_category);
        }

        // POST: CategoryAction/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            traqr_category traqr_category = db.traqr_category.Find(id);
            db.traqr_category.Remove(traqr_category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
