namespace TraQR.Web.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class InventoryModel : DbContext
    {
        public InventoryModel()
            : base("name=InventoryModel")
        {
        }

        public virtual DbSet<traqr_category> traqr_category { get; set; }
        public virtual DbSet<traqr_companies> traqr_companies { get; set; }
        public virtual DbSet<traqr_items> traqr_items { get; set; }
        public virtual DbSet<traqr_status> traqr_status { get; set; }
        public virtual DbSet<traqr_user> traqr_user { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<traqr_category>()
                .Property(e => e.CategoryName)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_companies>()
                .Property(e => e.CompanyName)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_companies>()
                .Property(e => e.Street)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_companies>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_companies>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_companies>()
                .Property(e => e.RegistrationCode)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_companies>()
                .Property(e => e.Zip)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_companies>()
                .Property(e => e.AptNumber)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_items>()
                .Property(e => e.ItemName)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_items>()
                .Property(e => e.ItemDescription)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_items>()
                .Property(e => e.Latitude)
                .HasPrecision(18, 0);

            modelBuilder.Entity<traqr_items>()
                .Property(e => e.Longitude)
                .HasPrecision(18, 0);

            modelBuilder.Entity<traqr_items>()
                .Property(e => e.LocationName)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_items>()
                .Property(e => e.LocationDescription)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_items>()
                .Property(e => e.StatusDescription)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_status>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_user>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_user>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_user>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<traqr_user>()
                .Property(e => e.LastName)
                .IsUnicode(false);
        }
    }
}
