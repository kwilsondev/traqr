﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraQR.Web.Models
{
    public class InventoryViewModel
    {
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string Category { get; set; }
        public int? OrderNumber { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }
        public string Company { get; set; }
        public string LocationName { get; set; }
        public string LocationDescription { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string Username { get; set; }
    }
}