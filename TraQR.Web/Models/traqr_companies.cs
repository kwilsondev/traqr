//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TraQR.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class traqr_companies
    {
        public int CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string RegistrationCode { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string AptNumber { get; set; }
    }
}
