﻿using System.Net.Http.Headers;
using System.Web.Http;

namespace TraQR.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Formatters.JsonFormatter.SupportedMediaTypes
            .Add(new MediaTypeHeaderValue("text/html"));

            // Web API routes
            config.MapHttpAttributeRoutes();

            //this sets up the default API route where we will call our api actions.
            //the action in the url will link to a method in APIController
            config.Routes.MapHttpRoute(
                name: "api",
                routeTemplate: "api/{action}",
                defaults: new { controller = "API" }
            );
        }
    }
}
