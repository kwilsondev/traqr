﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace TraQR.Web.Controllers
{
    public class APIController : ApiController
    {
        //-------------------------------------------------
        //| example method to return a simple JSON object |
        //|  when a call is made to localhost/api/print   |
        //-------------------------------------------------

        [System.Web.Http.HttpGet]    //exposes the method to GET requests
        [System.Web.Http.HttpPost]   //exposes the method to POST requests
        public JsonResult Print()
        {
            //create the JSON object to hold the results
            var json = new JsonResult();


            if ( Request.Method.Equals(HttpMethod.Get))             //check if request is a GET request
            {

                json.Data = "{ result: 'Success using a GET request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here
                json.JsonRequestBehavior = JsonRequestBehavior.AllowGet; // needed in the json if accessed from a GET request only

            } else if (Request.Method.Equals(HttpMethod.Post)) {    //check if request is a Post request
            
                json.Data = "{ result: 'Success using a POST request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            } else {

                json.Data = "{ result: 'Bad request' }"; //add data to the JSON object. We would usually be serializing a list of objects into JSON here

            }

            return json; //return the JSON object to the client

        }

    }
}
