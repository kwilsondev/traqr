﻿using System;
using Newtonsoft.Json;
using TraQR.Mobile.Models;
using Xamarin.Forms;

namespace TraQR.Mobile
{
    public partial class App : Application
    {
        public static ApiAccess apiAccess;
        public static traqr_user CurrentUser;
        public static MasterDetailPage MDP;

        public App()
        {
            InitializeComponent();

            apiAccess = new ApiAccess();
            MainPage = new LoginPage();

            if (Current.Properties.ContainsKey("CurrentUser") && !Current.Properties["CurrentUser"].Equals("-1"))
            {
                //init the current user from storage
                CurrentUser = JsonConvert.DeserializeObject<traqr_user>(Current.Properties["CurrentUser"].ToString());

                NavigationPage navPage = new NavigationPage(new InventoryPage());
                navPage.BackgroundColor = Color.FromHex("#333333");
                navPage.BarBackgroundColor = Color.FromHex("#262626");
                navPage.BarTextColor = Color.FromHex("#cc5200");

                MDP = new MasterDetailPage()
                {
                    Master = new MasterPage(CurrentUser.FirstName)
                    {
                        Title = "Menu",
                        BackgroundColor = Color.FromHex("#262626")
                    },

                    Detail = navPage
                };

                MainPage = MDP;

            }
            //else
            //{
            //    MainPage = new LoginPage();
            //}

        }

        protected override void OnStart()
        {
            
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
