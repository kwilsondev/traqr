﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Essentials;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using TraQR.Mobile.Models;

using Xamarin.Forms;

namespace TraQR.Mobile
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        async void LoginPressed(object sender, System.EventArgs e)
        {
            ApiAccess api = new ApiAccess();

            //Mobile Network access
            var current = Connectivity.NetworkAccess;
            //WiFi access
            var profiles = Connectivity.ConnectionProfiles;
            //keeps running until device has internet access
            while (true)
            {
                //device has mobile network access
                if (current == NetworkAccess.Internet)
                {
                    break;
                }
                //device has WiFi access
                if (profiles.Contains(ConnectionProfile.WiFi))
                {
                    break;
                }
                //Asks user to enable internet access
                await DisplayAlert("Error", "No Internet Access\nPlease enable internet access to continue.", "OK");
                current = Connectivity.NetworkAccess;
                profiles = Connectivity.ConnectionProfiles;
            }

            traqr_user returnedUser = App.apiAccess.Authenticate(usernameEntry.Text, passwordEntry.Text);

            if (returnedUser.UserID != -1)
            {

                //Set the new login as the stored user account
                Application.Current.Properties["CurrentUser"] = JsonConvert.SerializeObject(returnedUser);
                App.CurrentUser = returnedUser;

                NavigationPage navPage = new NavigationPage(new InventoryPage());
                navPage.BackgroundColor = Color.FromHex("#333333");
                navPage.BarBackgroundColor = Color.FromHex("#262626");
                navPage.BarTextColor = Color.FromHex("#cc5200");

                App.MDP = new MasterDetailPage()
                {
                    Master = new MasterPage(returnedUser.FirstName)
                    {
                        Title = "Menu",
                        BackgroundColor = Color.FromHex("#262626")
                    },



                    Detail = navPage
                };

                await Navigation.PushModalAsync(App.MDP);
            }
            else
            {
                errorLabel.Text = "Incorrect login credentials, try again.";
                errorLabel.IsVisible = true;

                double xLoc = errorLabel.TranslationX;
                double yLoc = errorLabel.TranslationY;

                await errorLabel.TranslateTo(xLoc + 20, yLoc, 25, null);
                await errorLabel.TranslateTo(xLoc - 20, yLoc, 25, null);
                await errorLabel.TranslateTo(xLoc + 20, yLoc, 25, null);
                await errorLabel.TranslateTo(xLoc - 20, yLoc, 25, null);
                await errorLabel.TranslateTo(xLoc + 20, yLoc, 25, null);
                await errorLabel.TranslateTo(xLoc - 20, yLoc, 25, null);
                await errorLabel.TranslateTo(xLoc, yLoc, 25, null);
            }


        }

    }
}
