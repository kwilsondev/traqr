﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace TraQR.Mobile
{
    public partial class MasterPage : ContentPage
    {

        Location userLocation;

        public MasterPage(string firstName)
        {
            InitializeComponent();
            loggedInUserLabel.Text = firstName;
            CheckLocationPermissionAsync();
            CheckCameraPermissionAsync();
        }

        void InventoryTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Inventory");

            App.MDP.IsPresented = false;

            InventoryPage newPage = new InventoryPage();

            if (Device.RuntimePlatform != Device.iOS)
            {
                newPage.Title = "Inventory";
            }

            NavigationPage pageWrapper = new NavigationPage(newPage);

            pageWrapper.BackgroundColor = Color.FromHex("#333333");
            pageWrapper.BarBackgroundColor = Color.FromHex("#262626");
            pageWrapper.BarTextColor = Color.FromHex("#cc5200");

            App.MDP.Master.Title = "Inventory";

            App.MDP.Detail = pageWrapper;
        }

        void DetailScanTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Detail Scan");

            App.MDP.IsPresented = false;

            DetailScanPage newPage = new DetailScanPage();

            if (Device.RuntimePlatform != Device.iOS)
            {
                newPage.Title = "Detail Scan";
            }

            NavigationPage pageWrapper = new NavigationPage(newPage);

            pageWrapper.BackgroundColor = Color.FromHex("#333333");
            pageWrapper.BarBackgroundColor = Color.FromHex("#262626");
            pageWrapper.BarTextColor = Color.FromHex("#cc5200");

            App.MDP.Master.Title = "Detail Scan";

            App.MDP.Detail = pageWrapper;
        }

        void ContinuousScanTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Continuous Scan");

            App.MDP.IsPresented = false;

            ContinuousScanPage newPage = new ContinuousScanPage();

            if (Device.RuntimePlatform != Device.iOS)
            {
                newPage.Title = "Continuous Scan";
            }

            NavigationPage pageWrapper = new NavigationPage(newPage);

            pageWrapper.BackgroundColor = Color.FromHex("#333333");
            pageWrapper.BarBackgroundColor = Color.FromHex("#262626");
            pageWrapper.BarTextColor = Color.FromHex("#cc5200");

            App.MDP.Master.Title = "Continuous Scan";

            App.MDP.Detail = pageWrapper;
        }

        void MapTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Map");

            App.MDP.IsPresented = false;

            MapPage newPage = new MapPage();

            if (Device.RuntimePlatform != Device.iOS)
            {
                newPage.Title = "Map";
            }

            NavigationPage pageWrapper = new NavigationPage(newPage);

            pageWrapper.BackgroundColor = Color.FromHex("#333333");
            pageWrapper.BarBackgroundColor = Color.FromHex("#262626");
            pageWrapper.BarTextColor = Color.FromHex("#cc5200");

            App.MDP.Master.Title = "Map";

            App.MDP.Detail = pageWrapper;
        }

        async void LogoutPressed(object sender, System.EventArgs e)
        {
            Application.Current.Properties["CurrentUser"] = "-1";
            await Application.Current.SavePropertiesAsync();

            await Navigation.PushModalAsync(new LoginPage());
            
        }

        //Gets Lat and Long coordinates of device 
        void GetLocation()
        {
            try
            {
                var task = Geolocation.GetLocationAsync();
                task.ContinueWith(x =>
                {
                    userLocation = x.Result;
                    if (userLocation != null)
                        DisplayAlert("Device Location", $"latitude:{userLocation.Latitude}, longitude:{userLocation.Longitude}", "OK");

                }, TaskScheduler.FromCurrentSynchronizationContext());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Checks if app can access location
        public async System.Threading.Tasks.Task CheckLocationPermissionAsync()
        {
            Permission permission = Permission.Location;
            try
            {
                PermissionStatus status = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
                if (status != PermissionStatus.Granted)
                {
                    await DisplayAlert(permission + " Permission Request", "In order for the app to access the device location, permission must first be granted.", "OK");
                    Dictionary<Permission, PermissionStatus> results = await CrossPermissions.Current.RequestPermissionsAsync(permission);
                    status = results[permission];
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("\nIn App.Helpers.NetworkHelper.CheckRequestPermissionAsync() - Exception attempting to check or request permission to Permission.{0}:\n{1}\n", permission, ex);
            }
        }

        //Checks if app can access camera
        public async System.Threading.Tasks.Task CheckCameraPermissionAsync()
        {
            Permission permission = Permission.Camera;
            try
            {
                PermissionStatus status = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
                if (status != PermissionStatus.Granted)
                {
                    //It has not been granted so lets ask
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(permission))
                    {
                        await DisplayAlert(permission + " Permission Request", "In order for the app to access the camera, permission must first be granted.", "OK");
                    }
                    Dictionary<Permission, PermissionStatus> results = await CrossPermissions.Current.RequestPermissionsAsync(permission);
                    status = results[permission];
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("\nIn App.Helpers.NetworkHelper.CheckRequestPermissionAsync() - Exception attempting to check or request permission to Permission.{0}:\n{1}\n", permission, ex);
            }
        }

    }
}
