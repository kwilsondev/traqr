﻿using System;
using System.Collections.Generic;
using System.Linq;
using TraQR.Mobile.Models;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace TraQR.Mobile
{
    public partial class ItemDetailsPage : ContentPage
    {

        List<traqr_category> categories;
        List<traqr_status> statuses;
        traqr_inventory currentItem;

        public ItemDetailsPage()
        {

        }

        public ItemDetailsPage(traqr_inventory item)
        {
            InitializeComponent();

            nameEntry.IsEnabled = false;
            descriptionEditor.IsEnabled = false;
            //categoryPicker.IsEnabled = false;

            ToolbarItem EditToggle = new ToolbarItem("Edit", null, EditToggleTapped, ToolbarItemOrder.Default, 0);
            ToolbarItems.Add(EditToggle);

            currentItem = item;

            categories = App.apiAccess.GetCategories();
            categories.Insert(0, new traqr_category {CategoryID = -1, CategoryName = "..." });
            statuses = App.apiAccess.GetStatuses();

            nameEntry.Text = item.ItemName;
            descriptionEditor.Text = item.ItemDescription;
            //categoryPicker.ItemsSource = categories;
            //categoryPicker.ItemDisplayBinding = new Binding("CategoryName");
            //categoryPicker.SelectedIndex = categories.IndexOf(categories.First(c => c.CategoryName == item.Category));
            statusLabel.Text = item.Status;
            scannedByLabel.Text = item.User;

            var mapView = new Map(
            MapSpan.FromCenterAndRadius(
                new Position((double)item.Latitude, (double)item.Longitude), Distance.FromMiles(0.3)))
                {
                    IsShowingUser = true,
                    HeightRequest = 100,
                    WidthRequest = 960,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };

            mapView.MapType = MapType.Hybrid;
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = new Position((double)item.Latitude, (double)item.Longitude),
                Label = item.ItemName,
                Address = item.ItemDescription
            };
            mapView.Pins.Add(pin);

            mainStack.Children.Add(mapView);


        }

        async void EditToggleTapped()
        {
            if(nameEntry.Text == "" || descriptionEditor.Text == "")
            {
                await DisplayAlert("Invalid Entry", "Please make sure that all editable items are filled out", "Okay");
                return;
            }

            if (ToolbarItems.ElementAt(0).Text == "Edit")
            {
                ToolbarItems.ElementAt(0).Text = "Submit";
                nameEntry.IsEnabled = true;
                descriptionEditor.IsEnabled = true;
                //categoryPicker.IsEnabled = true;
            }
            else
            {
                ToolbarItems.ElementAt(0).Text = "Edit";
                nameEntry.IsEnabled = false;
                descriptionEditor.IsEnabled = false;
                //categoryPicker.IsEnabled = false;

                var items = App.apiAccess.GetItems();
                traqr_item updatedItem = items.First(i => i.ItemID == currentItem.ItemID);
                updatedItem.ItemName = nameEntry.Text;
                updatedItem.ItemDescription = descriptionEditor.Text;
                //updatedItem.CategoryID = categories[categoryPicker.SelectedIndex].CategoryID;

                string result = App.apiAccess.UpdateItem(updatedItem);

                if (result == "\"\"")
                {
                    await DisplayAlert("Item Updated", "This inventory item has successfully been updated.", "Okay");
                }
                else
                {
                    await DisplayAlert("Item Updated", "This inventory item has successfully been updated.", "Okay");
                }
            }

        }

    }
}
