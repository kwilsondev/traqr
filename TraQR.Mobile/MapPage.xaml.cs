﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TraQR.Mobile.Models;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace TraQR.Mobile
{

	public partial class MapPage : ContentPage
	{

        List<traqr_inventory> inventory;

		public MapPage ()
		{ 
			InitializeComponent ();

            inventory = App.apiAccess.GetInventory();

            var mapView = new Map(
            MapSpan.FromCenterAndRadius(
                new Position(43,-78), Distance.FromMiles(250)))
            {
                IsShowingUser = true,
                HeightRequest = 100,
                WidthRequest = 960,
                VerticalOptions = LayoutOptions.FillAndExpand
            };


            foreach (var item in inventory)
            {
                var pin = new Pin
                {
                    Type = PinType.Place,
                    Position = new Position((double)item.Latitude, (double)item.Longitude),
                    Label = item.ItemName,
                    Address = item.ItemDescription
                };
                mapView.Pins.Add(pin);
            }

            mapView.MapType = MapType.Hybrid;


            Content = mapView;

        }
	}
}