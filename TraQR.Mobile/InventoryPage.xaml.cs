﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TraQR.Mobile.Models;
using Xamarin.Forms;


namespace TraQR.Mobile
{
    public partial class InventoryPage : ContentPage
    {

        List<traqr_inventory> inventory;

        public InventoryPage()
        {
            InitializeComponent();

            //get the inventory items associated with the current user's company
            inventory = App.apiAccess.GetInventory();

            Task.Run(() =>
           {
               PopulateInventoryView(inventory);
           });
        }

        void SearchTextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if(!string.IsNullOrEmpty(e.NewTextValue))
            {
                var refinedInventory = inventory.Where(i => i.ItemName.ToLower().Contains(e.NewTextValue.ToLower()) ||
                                                        i.ItemDescription.ToLower().Contains(e.NewTextValue.ToLower()) ||
                                                        i.Status.ToLower().Contains(e.NewTextValue.ToLower()));

                PopulateInventoryView(refinedInventory.ToList());
            }
            else
            {
                PopulateInventoryView(inventory);
            }

        }

        void PopulateInventoryView(List<traqr_inventory> inventoryIn)
        {

            InventoryStack.Children.Clear();

            foreach(traqr_inventory item in inventoryIn)
            {

                Frame f = new Frame
                {
                    BackgroundColor = Color.FromHex("#404040"),
                    BorderColor = Color.Black,
                    CornerRadius = 10,
                    Padding = 10,
                    Margin = new Thickness(10, 5)

                };

                StackLayout vStack = new StackLayout
                {
                    Orientation = StackOrientation.Vertical
                };

                StackLayout line1 = new StackLayout
                {
                    Orientation = StackOrientation.Horizontal,
                    HorizontalOptions = LayoutOptions.FillAndExpand
                };

                line1.Children.Add(new Label
                {
                    Text = item.ItemName,
                    BindingContext = item.ItemName,
                    FontSize = 18,
                    TextColor = Color.FromHex("#cc5200"),
                    FontAttributes = FontAttributes.Bold
                });

                line1.Children.Add(new Label
                {
                    Text = item.Status,
                    BindingContext = item.Status,
                    FontSize = 15,
                    TextColor = Color.White,
                    HorizontalOptions = LayoutOptions.EndAndExpand
                });

                vStack.Children.Add(line1);

                vStack.Children.Add(new BoxView { Color = Color.Black, HeightRequest = 2 });

                vStack.Children.Add(new Label
                {
                    Text = item.ItemDescription,
                    BindingContext = item.ItemDescription,
                    FontSize = 15,
                    TextColor = Color.White
                });

                //set border color by status
                if (item.Status == "In Storage")
                {
                    f.BorderColor = Color.DarkGray;
                }
                else if (item.Status == "Loaded")
                {
                    f.BorderColor = Color.Orange;
                }
                else if (item.Status == "Unloaded")
                {
                    f.BorderColor = Color.Green;
                }
                else if (item.Status == "Damaged")
                {
                    f.BorderColor = Color.Red;
                }
                else if (item.Status == "Out For Repair")
                {
                    f.BorderColor = Color.Blue;
                }



                f.Content = vStack;

#pragma warning disable CS0618 // Type or member is obsolete
                f.GestureRecognizers.Add(new TapGestureRecognizer((obj) => {
                    ItemDetailsPage itemDetailPage = new ItemDetailsPage(item);
                    itemDetailPage.Disappearing += ItemDetailPage_Disappearing;
                    this.Navigation.PushAsync(itemDetailPage);
                }));
#pragma warning restore CS0618 // Type or member is obsolete

                Device.BeginInvokeOnMainThread(() => {
                    InventoryStack.Children.Add(f);
                });

            }


        }


        void ItemDetailPage_Disappearing(object sender, EventArgs e)
        {
            //get the inventory items associated with the current user's company
            inventory = App.apiAccess.GetInventory();

            PopulateInventoryView(inventory);
        }

    }
}