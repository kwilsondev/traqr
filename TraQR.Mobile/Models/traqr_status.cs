﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TraQR.Mobile.Models
{
    public class traqr_status
    {
        public traqr_status()
        {

        }
        public int? StatusID { get; set; }
        public string Status { get; set; }
    }
}
