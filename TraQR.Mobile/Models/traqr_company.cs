﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TraQR.Mobile.Models
{
    public class traqr_company
    {
        public traqr_company()
        {

        }
        public int? CompanyID { get; set; }
        public string CompanyName { get; set; }
        public string RegistrationCode { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string AptNumber { get; set; }


    }
}
