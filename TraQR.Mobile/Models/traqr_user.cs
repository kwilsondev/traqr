﻿using System;
namespace TraQR.Mobile.Models
{
    public class traqr_user
    {

        public int UserID { get; set; }

        public int? CompanyID { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public traqr_user()
        {
        }
    }
}
