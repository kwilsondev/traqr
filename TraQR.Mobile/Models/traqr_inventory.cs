﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TraQR.Mobile.Models
{
    public class traqr_inventory
    {
        public traqr_inventory()
        {

        }
        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string Category { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public string User { get; set; }
        public int? OrderNumber { get; set; }
        public string LocationName { get; set; }
        public string LocationDescription { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }
        public string Company { get; set; }
    }


}
