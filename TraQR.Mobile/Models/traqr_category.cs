﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TraQR.Mobile.Models
{
    public class traqr_category
    {
        public traqr_category()
        {

        }
        public int? CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}
