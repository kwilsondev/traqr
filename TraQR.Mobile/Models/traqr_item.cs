﻿using System;
namespace TraQR.Mobile.Models
{
    public class traqr_item
    {
        public traqr_item()
        {
        }

        public int ItemID { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public int? CategoryID { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int? UserID { get; set; }
        public int? OrderNumber { get; set; }
        public string LocationName { get; set; }
        public string LocationDescription { get; set; }
        public int? StatusID { get; set; }
        public string StatusDescription { get; set; }
        public int? CompanyID { get; set; }

    }
}
