﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using TraQR.Mobile.Models;

namespace TraQR.Mobile
{
    public class ApiAccess
    {

        private string mainURI = "http://ec2-3-18-213-173.us-east-2.compute.amazonaws.com";

        public ApiAccess()
        {
        }

        public List<traqr_category> GetCategories()
        {
            HttpClient httpClient = new HttpClient();

            List<traqr_category> categories = new List<traqr_category>();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/category/GetAll"),
                Method = HttpMethod.Get
            };
            var task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    categories = JsonConvert.DeserializeObject<List<traqr_category>>(await response.Content.ReadAsStringAsync());
                });
            task.Wait();

            return categories;
        }

        public List<traqr_status> GetStatuses()
        {
            HttpClient httpClient = new HttpClient();

            List<traqr_status> statuses = new List<traqr_status>();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/statustable/GetAll"),
                Method = HttpMethod.Post
            };
            var task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    statuses = JsonConvert.DeserializeObject<List<traqr_status>>(await response.Content.ReadAsStringAsync());
                });
            task.Wait();

            return statuses;
        }

        public List<traqr_item> GetItems()
        {
            List<traqr_item> returnedItems = new List<traqr_item>();

            HttpClient httpClient = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/item/getall"),
                Method = HttpMethod.Post
            };

            var task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    returnedItems = JsonConvert.DeserializeObject<List<traqr_item>>(await response.Content.ReadAsStringAsync());

                });
            task.Wait();


            return returnedItems;
        }

        public traqr_user Authenticate(string userName, string password)
        {

            traqr_user returnedUser = new traqr_user();

            HttpClient httpClient = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/Registration/authenticate"),
                Method = HttpMethod.Post
            };

            request.Headers.Add("UserName", userName);
            request.Headers.Add("Password", GetSha256(password));

            var task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    returnedUser = JsonConvert.DeserializeObject<traqr_user>(await response.Content.ReadAsStringAsync());

                });
            task.Wait();


            return returnedUser;

        }

        public string UpdateItem(traqr_item updatedItem)
        {
            string result = "";

            HttpClient httpClient = new HttpClient();
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/item/update"),
                Method = HttpMethod.Post
            };

            request.Headers.Add("ItemID", updatedItem.ItemID.ToString());
            request.Headers.Add("ItemName", updatedItem.ItemName);
            request.Headers.Add("ItemDescription", updatedItem.ItemDescription);
            request.Headers.Add("CategoryID", updatedItem.CategoryID.ToString());
            request.Headers.Add("Latitude", string.Format("{0:0.000000}", updatedItem.Latitude));
            request.Headers.Add("Longitude", string.Format("{0:0.000000}", updatedItem.Longitude));
            request.Headers.Add("UserID", updatedItem.UserID.ToString());
            request.Headers.Add("OrderNumber", updatedItem.OrderNumber.ToString());
            request.Headers.Add("LocationName", updatedItem.LocationName);
            request.Headers.Add("LocationDescription", updatedItem.LocationDescription);
            request.Headers.Add("StatusID", updatedItem.StatusID.ToString());
            request.Headers.Add("StatusDescription", updatedItem.StatusDescription);
            request.Headers.Add("CompanyID", updatedItem.CompanyID.ToString());

            var task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    result = await response.Content.ReadAsStringAsync();

                });
            task.Wait();



            return result;
        }

        //gets all items that are in item DB that have the same company as logged in user 
        //List has names for status, company, category, and user instead of their respective ID fields
        public List<traqr_inventory> GetInventory()
        {

            List <traqr_item> items = new List<traqr_item>();
            List <traqr_inventory> inventory = new List<traqr_inventory>();
            traqr_inventory inventoryItem;

            HttpClient httpClient = new HttpClient();


            //gets all items in item DB
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/item/GetAll"),
                Method = HttpMethod.Post
            };
            var task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    items = JsonConvert.DeserializeObject<List<traqr_item>>(await response.Content.ReadAsStringAsync());
                });
            task.Wait();



            //gets user row of logged in user to use company ID to filter inventory list
            traqr_user user = new traqr_user();
            request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/user/GetByUsername"),
                Method = HttpMethod.Post
            };

            request.Headers.Add("Username", App.CurrentUser.Username);

            task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    user = JsonConvert.DeserializeObject<traqr_user>(await response.Content.ReadAsStringAsync());
                });
            task.Wait();



            //gets company of logged in user
            traqr_company company = new traqr_company();
            request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/company/GetByCompanyID"),
                Method = HttpMethod.Post
            };

            request.Headers.Add("CompanyID", App.CurrentUser.CompanyID.ToString());

            task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    company = JsonConvert.DeserializeObject<traqr_company>(await response.Content.ReadAsStringAsync());
                });
            task.Wait();



            //Gets all statuses
            List<traqr_status> statuses = new List<traqr_status>();
            request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/statustable/GetAll"),
                Method = HttpMethod.Post
            };
            task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    statuses = JsonConvert.DeserializeObject<List<traqr_status>>(await response.Content.ReadAsStringAsync());
                });
            task.Wait();



            //Gets all categories
            List<traqr_category> categories = new List<traqr_category>();
            request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/categorytable/GetAll"),
                Method = HttpMethod.Post
            };
            task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    categories = JsonConvert.DeserializeObject<List<traqr_category>>(await response.Content.ReadAsStringAsync());
                });
            task.Wait();



            //Gets all users
            List<traqr_user> users = new List<traqr_user>();
            request = new HttpRequestMessage
            {
                RequestUri = new Uri(mainURI + "/api/user/GetAll"),
                Method = HttpMethod.Post
            };
            task = httpClient.SendAsync(request)
                .ContinueWith(async (taskwithmsg) =>
                {
                    var response = taskwithmsg.Result;
                    users = JsonConvert.DeserializeObject<List<traqr_user>>(await response.Content.ReadAsStringAsync());
                });
            task.Wait();



            //for each item in item DB
            foreach (traqr_item item in items)
            {
                if (item.CompanyID.Equals(user.CompanyID))
                {
                    inventoryItem = new traqr_inventory();
                    inventoryItem.ItemID = item.ItemID;
                    inventoryItem.ItemDescription = item.ItemDescription;
                    foreach (traqr_category category in categories)
                    {
                        if (item.CategoryID.Equals(category.CategoryID))
                        {
                            inventoryItem.Category = category.CategoryName;
                        }
                    }
                    foreach (traqr_status status in statuses)
                    {
                        if (item.StatusID.Equals(status.StatusID))
                        {
                            inventoryItem.Status = status.Status;
                        }
                    }
                    foreach (traqr_user _User in users)
                    {
                        if (_User.UserID.Equals(item.UserID))
                        {
                            inventoryItem.User = _User.Username;
                        }
                    }
                    inventoryItem.Company = company.CompanyName;
                    inventoryItem.ItemName = item.ItemName;
                    inventoryItem.Latitude = item.Latitude;
                    inventoryItem.LocationDescription = item.LocationDescription;
                    inventoryItem.LocationName = item.LocationName;
                    inventoryItem.Longitude = item.Longitude;
                    inventoryItem.OrderNumber = item.OrderNumber;
                    inventoryItem.StatusDescription = item.StatusDescription;

                    inventory.Add(inventoryItem);
                }
            }
            return inventory;
        }

        static string GetSha256(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

    }
}
