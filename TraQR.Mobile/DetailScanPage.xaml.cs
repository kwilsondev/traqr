﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TraQR.Mobile.Models;
using Xamarin.Essentials;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace TraQR.Mobile
{
	public partial class DetailScanPage : ContentPage
	{

        List<traqr_inventory> items;

        public DetailScanPage ()
		{
            InitializeComponent();
           

            items = App.apiAccess.GetInventory();

            ZXingScannerView scanPage = new ZXingScannerView();
            scanPage.IsScanning = true;
            scanPage.Options.DelayBetweenContinuousScans = 5000;
            var overlay = new ZXingDefaultOverlay
            {
                TopText = "Hold your phone up to the QR Code",
                BottomText = "Scanning will happen automatically",
                ShowFlashButton = scanPage.HasTorch,
                AutomationId = "continuousOverlay",
            };

            overlay.FlashButtonClicked += (sender, e) => {
                scanPage.IsTorchOn = !scanPage.IsTorchOn;
            };

            scanPage.OnScanResult += (result) =>
            {
                //scanPage.IsScanning = false;

                //pause scan until the current item is processed
                //scanPage.IsScanning = false;
                var duration = TimeSpan.FromSeconds(.15);
                Vibration.Vibrate(duration);

                //split the results to get the item name and id
                var scannedData = result.Text.Split(',');

                //get a reference to the item that is being uodated
                traqr_inventory updatedItem = items.FirstOrDefault(i => i.ItemID.ToString() == scannedData.ElementAt(0));

                if (updatedItem == null)
                {
                    Device.BeginInvokeOnMainThread( () => DisplayAlert("Invalid Code", "That QR code does not match any data for your company. Try again.", "Okay"));
                    //scanPage.IsScanning = true;
                    return;
                }

                Debug.WriteLine(updatedItem.ItemName);

                Device.BeginInvokeOnMainThread(async () => await Navigation.PushAsync(new ItemDetailsPage(updatedItem)));



            };

            overlay.VerticalOptions = LayoutOptions.FillAndExpand;
            scangrid.Children.Add(scanPage);
            scangrid.Children.Add(overlay);

        }
	}
}