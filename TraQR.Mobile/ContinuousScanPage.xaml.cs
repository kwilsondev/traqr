﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TraQR.Mobile.Models;
using Xamarin.Essentials;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace TraQR.Mobile
{
    public partial class ContinuousScanPage : ContentPage
    {

        List<traqr_item> items;
        List<traqr_status> statuses;

        public ContinuousScanPage()
        {
            InitializeComponent();

            //get a current list of items from the db
            items = App.apiAccess.GetItems();

            //get the current available statuses from the DB
            statuses = App.apiAccess.GetStatuses();
            statuses.Insert(0, new traqr_status {StatusID = -1, Status = "..." });
            statusPicker.ItemsSource = statuses;
            statusPicker.ItemDisplayBinding = new Binding("Status");
            statusPicker.SelectedIndex = 0;

            ZXingScannerView scanPage = new ZXingScannerView();
            scanPage.AutomationId = "continuousOverlay";
            scanPage.Options.DelayBetweenContinuousScans = 2000;
            scanPage.IsScanning = true;

            var overlay = new ZXingDefaultOverlay
            {
                TopText = "Hold your phone up to the QR Code",
                BottomText = "Scanning will happen automatically",
                ShowFlashButton = scanPage.HasTorch,
                AutomationId = "continuousOverlay",
            };

            overlay.FlashButtonClicked += (sender, e) => {
                scanPage.IsTorchOn = !scanPage.IsTorchOn;
            };
            scanPage.OnScanResult += async (result) =>
            {
        
                //pause scan until the current item is processed
                //scanPage.IsScanning = false;
                var duration = TimeSpan.FromSeconds(.15);
                Vibration.Vibrate(duration);

                //split the results to get the item name and id
                var scannedData = result.Text.Split(',');

                //get a reference to the item that is being uodated
                traqr_item updatedItem = items.First(i => i.ItemID.ToString() == scannedData.ElementAt(0));

                //get the user's current location
                Location location = new Location();
                try
                {
                    var locationRequest = new GeolocationRequest(GeolocationAccuracy.Best);
                    location = await Geolocation.GetLocationAsync(locationRequest);
                }
                catch (FeatureNotEnabledException fneEx)
                {
                    Device.BeginInvokeOnMainThread( async () => await DisplayAlert("Location Error","Location services are not enabled on your device. Enable them before scanning","Okay"));
                    return;
                }
                catch (PermissionException pEx)
                {
                    Device.BeginInvokeOnMainThread(async () => await DisplayAlert("Location Error", "TraQR does not have permission to use location services. Allow permission in your device settings before scanning.", "Okay"));
                    return;
                }
                catch (Exception ex)
                {
                    Device.BeginInvokeOnMainThread(async () => await DisplayAlert("Location Error", "An unknown error occurred within location services. Please try again.", "Okay"));
                    return;
                }

                //update the relevant item data
                if (statuses.ElementAt(statusPicker.SelectedIndex).StatusID != -1)
                {
                    updatedItem.StatusID = statuses.ElementAt(statusPicker.SelectedIndex).StatusID;
                    updatedItem.StatusDescription = statuses.ElementAt(statusPicker.SelectedIndex).Status;
                }
                else
                {
                    Device.BeginInvokeOnMainThread(async () => {
                        await DisplayAlert("Invalid Status", "Please choose a valid status before scanning", "Okay");
                    });
                    return;
                }
                updatedItem.Latitude = (decimal?)location.Latitude;
                updatedItem.Longitude = (decimal?)location.Longitude;

                Debug.WriteLine(updatedItem.Latitude.ToString());
                Debug.WriteLine(updatedItem.Longitude.ToString());


                //send the new item to the API for processing
                string updateResult = App.apiAccess.UpdateItem(updatedItem);

                Device.BeginInvokeOnMainThread(async () =>
                {
                    if(updateResult == "\"\"")
                    {
                        scanResultsLabel.Text = "Successfully updated location and status of Item #" + updatedItem.ItemID + ", " + updatedItem.ItemName;
                        scanResultsLabel.TextColor = Color.DarkGreen;
                    }
                    else
                    {
                        scanResultsLabel.Text = "Update to Item #" + updatedItem.ItemID + ", " + updatedItem.ItemName + " was unsuccessful";
                        scanResultsLabel.TextColor = Color.Maroon;

                    }

                    double xLoc = scanResultsLabel.TranslationX;
                    double yLoc = scanResultsLabel.TranslationY;

                    await scanResultsLabel.TranslateTo(xLoc + 20, yLoc, 25, null);
                    await scanResultsLabel.TranslateTo(xLoc - 20, yLoc, 25, null);
                    await scanResultsLabel.TranslateTo(xLoc + 20, yLoc, 25, null);
                    await scanResultsLabel.TranslateTo(xLoc - 20, yLoc, 25, null);
                    await scanResultsLabel.TranslateTo(xLoc + 20, yLoc, 25, null);
                    await scanResultsLabel.TranslateTo(xLoc - 20, yLoc, 25, null);
                    await scanResultsLabel.TranslateTo(xLoc, yLoc, 25, null);

                });

                //scanPage.IsScanning = true;

            };

            scanGrid.Children.Add(scanPage);
            scanGrid.Children.Add(overlay);

        }

        async void StartScanningPressed(object sender, System.EventArgs e)
        {

            ZXingScannerPage scanPage = new ZXingScannerPage();
            scanPage.DefaultOverlayTopText = "Inventory QR";
            scanPage.DefaultOverlayBottomText = "Focus on a code to scan";
            scanPage.IsScanning = true;
            scanPage.OnScanResult += (result) =>
            {
                //scanPage.IsScanning = false;
                Device.BeginInvokeOnMainThread(() =>
                {
                    var duration = TimeSpan.FromSeconds(.25);
                    Vibration.Vibrate(duration);

                    scanPage.DefaultOverlayTopText = result.Text;
                });
            };

            await Navigation.PushModalAsync(scanPage);
        }

    }
}
